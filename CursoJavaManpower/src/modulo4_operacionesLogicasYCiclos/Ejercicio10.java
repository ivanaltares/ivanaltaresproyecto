package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio10 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir variable 1: ");
		int num1 = sc.nextInt();
		System.out.println("Introducir variable 2: ");
		int num2 = sc.nextInt();
		System.out.println("Introducir variable 3: ");
		int num3 = sc.nextInt();

		if(num1>=num2 && num1>=num3)
				System.out.println("El numero "+num1+" es el mayor. ");
		else if(num1>=num2 && num3>=num1)
				System.out.println("El numero "+num3+" es el mayor. ");
		else if(num2>=num1 && num2>=num3)
				System.out.println("El numero "+num2+" es el mayor. ");
		else if(num2>=num1 && num3>=num2)
				System.out.println("El numero "+num3+" es el mayor. ");

		}
}
