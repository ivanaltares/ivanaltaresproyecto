package modulo8_objetosFigura.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modulo8_objetosFigura.Circulo;
import modulo8_objetosFigura.Cuadrado;
import modulo8_objetosFigura.Figura;
import modulo8_objetosFigura.Rectangulo;
import modulo8_objetosFigura.Triangulo;

class FiguraTest {

	Figura cir, cua, rec, tri, cirVacio, cuaVacio, recVacio, triVacio;
	Set<Figura> setFiguras;
	List<Figura> listaFiguras;
	
	@BeforeEach
	void setUp() throws Exception {
		cir=new Circulo("circulo", 3);
		cua=new Cuadrado("cuadrado", 4);
		rec=new Rectangulo("rectangulo", 4, 4);
		tri=new Triangulo("triangulo", 2, 2, 2);
		cirVacio=new Circulo();
		cuaVacio=new Cuadrado();
		recVacio=new Rectangulo();
		triVacio=new Triangulo();
		
		setFiguras=new HashSet<Figura>();
		setFiguras.add(new Circulo());
		setFiguras.add(new Cuadrado());
		
		listaFiguras=new ArrayList<Figura>();
		listaFiguras.add(new Triangulo());
		listaFiguras.add(new Rectangulo());
	}

	@AfterEach
	void tearDown() throws Exception {
		cir=null;
		cua=null;
		rec=null;
		tri=null;
		cirVacio=null;
		cuaVacio=null;
		recVacio=null;
		triVacio=null;
		
		setFiguras=null;
		listaFiguras=null;
	}
	
	@Test
	void testPerimetroCirculo() {
		assertTrue(cir.calcularPerimetro()==(float)(2*Math.PI*3));
	}
	
	@Test
	void testPerimetroCuadrado() {
		assertTrue(cua.calcularPerimetro()==(float)(4*4));
	}
	
	@Test
	void testPerimetroRectangulo() {
		assertTrue(rec.calcularPerimetro()==(float)(4+4+4+4));
	}
	
	@Test
	void testPerimetroTriangulo() {
		assertTrue(tri.calcularPerimetro()==(float)(2+2+2));
	}
	
	@Test
	void testSuperficieCirculo() {
		String resul2=String.format("%.4f", cir.calcularSuperficie());
		String resulEsperado2=String.format("%.4f", Math.PI*3*3);
		assertEquals(resulEsperado2, resul2);
	}
	
	@Test
	void testSuperficieCuadrado() {
		assertEquals(cua.calcularSuperficie(),4*4);
	}
	
	@Test
	void testSuperficieRectangulo() {
		assertEquals(rec.calcularSuperficie(),4*4);
	}
	
	@Test
	void testSuperficieTriangulo() {
		String resul=String.format("%.4f", tri.calcularSuperficie());
		String resulEsperado=String.format("%.4f", 1.7321f);
		assertEquals(resulEsperado,resul);
	}
	
	@Test
	void testSetFiguras() {
		setFiguras.add(new Cuadrado("cuadrao", 1));
		assertTrue(setFiguras.size()==3);
	}
	
	@Test
	void testListFiguras() {
		listaFiguras.add(new Circulo("circuloAux", 4));
		assertTrue(listaFiguras.size()==3);
	}
	
	@Test
	void testInstanciarCirculoVacio() {
		Figura cirAux=new Circulo();
		assertEquals(cirAux, cirVacio);
	}
	
	@Test
	void testInstanciarCuadradoVacio() {
		Figura cuaAux=new Cuadrado();
		assertEquals(cuaAux, cuaVacio);
	}
	
	@Test
	void testInstanciarRectanguloVacio() {
		Figura recAux=new Rectangulo();
		assertEquals(recAux, recVacio);
	}
	
	@Test
	void testInstanciarTrianguloVacio() {
		Figura triAux=new Triangulo();
		assertEquals(triAux, triVacio);
	}
	
	@Test
	void testInstanciarCirculo() {
		assertFalse(cir==null);
	}
	
	@Test
	void testInstanciarCuadrado() {
		assertFalse(cua==null);
	}
	
	@Test
	void testInstanciarRectangulo() {
		assertFalse(rec==null);
	}
	
	@Test
	void testInstanciarTriangulo() {
		assertFalse(tri==null);
	}

	@Test
	void testEqualsCirculo() {
		Figura cir2=new Circulo("circulo", 3);
		assertEquals(cir, cir2);
	}
	
	@Test
	void testEqualsCuadrado() {
		Figura cua2=new Cuadrado("cuadrado", 4);
		assertEquals(cua, cua2);
	}
	
	@Test
	void testEqualsRectangulo() {
		Figura rec2=new Rectangulo("rectangulo", 4, 4);
		assertEquals(rec, rec2);
	}
	
	@Test
	void testEqualsTriangulo() {
		Figura tri2=new Triangulo("triangulo", 2, 2, 2);
		assertEquals(tri, tri2);
	}
	
	
	
}
