package modulo6_staticYFechas;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	//en la practica todos los metodos carecen de modificador de acceso asi que
	//asumo que son public al ser metodos estaticos y poder ser utilizados de otras clases

	public static int getAnio(Date fecha) {
		Calendar cal=Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.YEAR);
	}

	public static int getMes(Date fecha) {
		Calendar cal=Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.MONTH);
	}

	public static int getDia(Date fecha) {
		Calendar cal=Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.DATE);
	}

	public static boolean isFinDeSemana(Date fecha) {
		Calendar cal=Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.DAY_OF_WEEK)==0 || cal.get(Calendar.DAY_OF_WEEK)==6;
	}

	public static boolean isDiaDeSemana(Date fecha) {
		Calendar cal=Calendar.getInstance();
		cal.setTime(fecha);
		return !(cal.get(Calendar.DAY_OF_WEEK)==0 || cal.get(Calendar.DAY_OF_WEEK)==6);
	}

	public static int getDiaDeSemana(Date fecha) {
		Calendar cal=Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	public static Date asDate(String pattern, String fecha) {
		SimpleDateFormat formato=new SimpleDateFormat(pattern);
		Date d=null;
		try {
			d= formato.parse(fecha);
		} catch (ParseException e) {
			System.out.println("Error parseando la fecha. ");
			e.printStackTrace();
		}

		return d;
	}

	public static Calendar asCalendar(String pattern, String fecha) {
		SimpleDateFormat formato=new SimpleDateFormat(pattern);
		Calendar calendario=Calendar.getInstance();

		try {
			calendario.setTime(formato.parse(fecha));
		} catch (ParseException e) {
			System.out.println("Error parseando la fecha calendario. ");
			e.printStackTrace();
		}

		return calendario;
	}

	public static String asString(String pattern, Date fecha) {
		SimpleDateFormat formato=new SimpleDateFormat(pattern);
		return formato.format(fecha);
	}
}
