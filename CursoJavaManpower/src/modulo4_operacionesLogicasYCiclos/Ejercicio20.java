package modulo4_operacionesLogicasYCiclos;

public class Ejercicio20 {

	public static void main(String[] args) {

		int max=51;
		int min=0;
		int valor;
		int i=0;
		System.out.println("Seleccionando valores del 1 al 50... \n");
		//cojo valores aleatorios del 1 al 50

		do {
			valor=1 + (int)(Math.random() * ((50 - 1) + 1));
			System.out.println("Numero elegido: "+valor);
			if(i==0) {
				max=valor;
				min=valor;
			}
			else {
				if(valor>max)
					max=valor;
				if(valor<min)
					min=valor;
			}

			i++;
		}while(i<11);


		System.out.println("\nMaximo: "+max);
		System.out.println("Minimo: "+min);

	}

}
