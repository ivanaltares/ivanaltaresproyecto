package modulo8_objetosFigura;

import java.util.Objects;

public abstract class Figura {

	protected static float maximaSuperficie;
	protected String nombre;
	
	public Figura() {
		maximaSuperficie=0;
		nombre="";
	}

	public Figura(String nombre) {
		this.nombre = nombre;
	}
	
	public abstract float calcularPerimetro();
	
	public abstract float calcularSuperficie();
	
	public static float getMaximaSuperficie() {
		return maximaSuperficie;
	}

	public static void setMaximaSuperficie(float maximaSuperficie) {
		Figura.maximaSuperficie = maximaSuperficie;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public abstract String getValores();

	@Override
	public int hashCode() {
		return Objects.hash(nombre);
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Figura &&
				((Figura)obj).getNombre().equals(nombre);
	}

	@Override
	public String toString() {
		return "Figura: nombre=" + nombre + ", maxima superficie=";
	}
	
	
	
	
}
