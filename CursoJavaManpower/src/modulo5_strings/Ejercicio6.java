package modulo5_strings;

import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.println("Introducir cadena de texto: ");
		String texto=sc.nextLine();

		boolean tieneNum=false;
		for(int i=0; i<texto.length(); i++) {
			//utilizo la tabla ascii para no tener 10 condiciones
			if(texto.charAt(i)>=48 && texto.charAt(i)<=57)
				tieneNum=true;
		}

		if(tieneNum)
			System.out.println("La cadena tiene numeros. ");
		else
			System.out.println("La cadena no tiene numeros. ");
	}

}
