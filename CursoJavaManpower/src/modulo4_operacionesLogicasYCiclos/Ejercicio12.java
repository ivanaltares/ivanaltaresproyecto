package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir entero: ");
		int num = sc.nextInt();

		if(num<1 || num>36)
			System.out.println("El numero "+num+" esta fuera de rango. ");
		else if(num>=1 && num<=12)
			System.out.println("El numero "+num+" esta en la primera docena. ");
		else if(num>=13 && num<=24)
			System.out.println("El numero "+num+" esta en la segunda docena. ");
		else
			System.out.println("El numero "+num+" esta en la tercera docena. ");
	}

}
