package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir el numero (parte a): ");
		int num = sc.nextInt();

		int suma=0;

		for(int i=1; i<11; i++) {
			System.out.println(num+" \tx \t"+i+" \t= \t"+num*i);
			if((num*i)%2==0)
				suma+=num*i;
		}

		System.out.println("Suma de los valores pares: "+suma);

		System.out.println("*************************************");

		System.out.println("Introducir el numero (parte b): ");
		int num2 = sc.nextInt();

		int suma2=0;

		for(int i=1; i<11; i++) {
			System.out.println(num2+" \tx \t"+i+" \t= \t"+num2*i);
			suma2+=(-(((num2*i)%2)-1))*(num2*i);
		}



		System.out.println("Suma de los valores pares: "+suma2);

	}

}
