package modulo7_objetosPersona;

import java.util.Objects;

public class Alumno extends Persona {

	private int legajo;


	public Alumno() {
		super();
		legajo=0;
	}

	public Alumno(int legajo) {
		super();
		this.legajo=legajo;
	}

	public Alumno(String nombre, String apellido, int legajo) {
		super(nombre, apellido);
		this.legajo=legajo;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.getApellido(),super.getNombre(),legajo);
	}

	@Override
	public boolean equals(Object obj) {

		return obj instanceof Alumno && super.equals(obj) && this.legajo==((Alumno)obj).getLegajo();
	}

	@Override
	public String toString() {
		return "Alumno:\tApellido=" + super.getApellido() + ",\tNombre=" + super.getNombre() + ",\tLegajo="+legajo+". ";
	}

}
