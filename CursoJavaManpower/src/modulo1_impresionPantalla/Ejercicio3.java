package modulo1_impresionPantalla;

public class Ejercicio3 {

	public static void main(String[] args) {

		//ejercicio para mostrar los caracteres especiales por pantalla
		System.out.println("Tecla de escape\t\tsignificado\n");
		System.out.println("\\n\t\t\tsignifica nueva l�nea");
		System.out.println("\\t\t\t\tsignifica un tab de espacio");
		System.out.println("\\\"\t\t\tes para poner \" (comillas dobles) dentro del texto por ejemplo \"Belencita\"");
		System.out.println("\\\\\t\t\tse utiliza para escribir la \\ dentro del texto, por ejemplo \\algo\\");
		System.out.println("\\\'\t\t\tse utiliza para las \'(comilla simple) para escribir por ejemplo \'Princesita\'");
	}

}
