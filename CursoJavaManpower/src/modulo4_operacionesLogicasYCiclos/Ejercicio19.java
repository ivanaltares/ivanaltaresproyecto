package modulo4_operacionesLogicasYCiclos;

public class Ejercicio19 {

	public static void main(String[] args) {
		int suma=0;
		int i=0;
		int valor;
		System.out.println("Seleccionando valores del 1 al 50... \n");
		//cojo valores aleatorios del 1 al 50
		while(i<11) {
			valor=1 + (int)(Math.random() * ((50 - 1) + 1));
			suma+=valor;
			System.out.println("Numero elegido: "+valor);
			i++;
		}

		System.out.println("\nSuma total: "+suma);
		System.out.println("Promedio: "+suma/10);

	}

}
