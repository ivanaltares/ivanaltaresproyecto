package modulo8_objetosFigura;

import java.util.Objects;

public class Triangulo extends Figura {

	private float lado1, lado2, lado3;

	public Triangulo() {
		super();
		lado1=0;
		lado2=0;
		lado3=0;
	}

	public Triangulo(String nombre) {
		super(nombre);
		lado1=0;
		lado2=0;
		lado3=0;
	}
	
	public Triangulo(String nombre, float lado1, float lado2, float lado3) {
		super(nombre);
		this.lado1=lado1;
		this.lado2=lado2;
		this.lado3=lado3;
	}
	
	

	public float getLado1() {
		return lado1;
	}

	public void setLado1(float lado1) {
		this.lado1 = lado1;
	}

	public float getLado2() {
		return lado2;
	}

	public void setLado2(float lado2) {
		this.lado2 = lado2;
	}

	public float getLado3() {
		return lado3;
	}

	public void setLado3(float lado3) {
		this.lado3 = lado3;
	}

	@Override
	public float calcularPerimetro() {
		return lado1+lado2+lado3;
	}

	@Override
	public float calcularSuperficie() {
		if((lado1+lado2)>lado3 && (lado1+lado3)>lado2 && (lado2+lado3)>lado1) {
			float aux=(lado1+lado2+lado3)/2;
			float resul=(float)(Math.sqrt(aux*(aux-lado1)*(aux-lado2)*(aux-lado3)));
			if(resul>Figura.maximaSuperficie)
				Figura.setMaximaSuperficie(resul);
			return resul;
		}
		else
			return -1;
	}

	@Override
	public String getValores() {
		return "lado1= "+lado1+", lado2="+lado2+", lado3="+lado3;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(lado1, lado2, lado3);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Triangulo 			&&
				super.equals(obj)					&&
				((Triangulo)obj).getLado1()==lado1	&& 
				((Triangulo)obj).getLado2()==lado2	&&
				((Triangulo)obj).getLado3()==lado3;
	}


	@Override
	public String toString() {
		return "Triangulo: lado1=" + lado1 + ", lado2=" + lado2 + ", lado3=" + lado3;
	}

	
	
}
