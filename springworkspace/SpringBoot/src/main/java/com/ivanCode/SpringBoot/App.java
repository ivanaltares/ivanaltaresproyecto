package com.ivanCode.SpringBoot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ivanCode.beans.Mundo;

/**
 * Hello world!
 *
 */
public class App{
    public static void main( String[] args )  {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("com/ivanCode/xml/beans.xml");
        Mundo m = (Mundo)appContext.getBean("mundo");
        
        ((ConfigurableApplicationContext)appContext).close();
    }
}
