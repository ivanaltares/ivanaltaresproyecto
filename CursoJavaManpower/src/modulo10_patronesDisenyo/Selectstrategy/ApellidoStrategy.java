package modulo10_patronesDisenyo.Selectstrategy;

public class ApellidoStrategy extends SelectStrategy {

	public ApellidoStrategy() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCondicion() {
		StringBuilder sb = new StringBuilder("");
		if(tengoWhere) {
			sb.append(" and alu_apellido=");
		}
		else {
			sb.append(" where alu_apellido=");
			tengoWhere=true;
		}
			
			
		sb.append(alumno.getApellido());
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		//tengoWhere =( alumno.getApellido()!=null && !alumno.getApellido().isEmpty());
		return (alumno.getApellido()!=null && !alumno.getApellido().isEmpty());
	}

}
