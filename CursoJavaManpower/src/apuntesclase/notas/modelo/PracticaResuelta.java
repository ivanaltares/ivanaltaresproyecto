package apuntesclase.notas.modelo;

import java.util.Objects;

public class PracticaResuelta implements Model {
	private int codigo;
	private int nota;
	private String observaciones;
	private int codAlu;
	private int codPrac;

	public PracticaResuelta() {}
	
	public PracticaResuelta(int cod) {
		this.codigo=cod;
		nota=0;
		observaciones="";
		codAlu=0;
		codPrac=0;
	}

	public PracticaResuelta(int codigo, int codPrac, int codAlu, int nota, String observaciones) {
		super();
		this.codigo = codigo;
		this.codPrac=codPrac;
		this.codAlu=codAlu;
		this.nota = nota;
		this.observaciones = observaciones;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public int getCodAlu() {
		return codAlu;
	}

	public void setCodAlu(int codAlu) {
		this.codAlu = codAlu;
	}

	public int getCodPrac() {
		return codPrac;
	}

	public void setCodPrac(int codPrac) {
		this.codPrac = codPrac;
	}

	@Override
	public boolean equals(Object obj){
		return obj instanceof Alumno 							&&
				((PracticaResuelta)obj).getCodigo()==codigo   	&&
				((PracticaResuelta)obj).getNota()==nota 		&&
				((PracticaResuelta)obj).getObservaciones().equals(observaciones) &&
				((PracticaResuelta)obj).getCodAlu()==codAlu 	&&
				((PracticaResuelta)obj).getCodPrac()==codPrac; 
	}
	@Override
	public int hashCode(){
		return Objects.hash(codigo, codPrac, codAlu, nota, observaciones);
	}

	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder("codigo=");
		sb.append(this.codigo);
		sb.append(", cod_practica=");
		sb.append(this.codPrac);
		sb.append(", cod_alumno=");
		sb.append(this.codAlu);
		sb.append(", nota=");
		sb.append(this.nota);
		sb.append(", observaciones=");
		sb.append(this.observaciones);

		return sb.toString();
	}

}
