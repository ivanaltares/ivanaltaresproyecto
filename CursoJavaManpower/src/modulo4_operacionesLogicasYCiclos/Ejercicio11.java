package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir caracter: ");
		char var = sc.nextLine().charAt(0);

		if(var=='a' || var=='e' || var=='i' || var=='o' || var=='u')
			System.out.println("Es una vocal. ");
		else
			System.out.println("Es una consonante. ");

	}

}
