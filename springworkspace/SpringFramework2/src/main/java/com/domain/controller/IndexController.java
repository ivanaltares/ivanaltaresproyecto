package com.domain.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.domain.modelo.Alumno;
import com.domain.modelo.dao.AlumnoDAO;

@Controller
public class IndexController {

	@RequestMapping("/home")
	public String goIndex() {
		return "Index";
	}

	@RequestMapping("/")
	public String goPresentacion() {
		return "Presentacion";
	}

	@RequestMapping("/listado")
	public String goListado(Model model) {
		
		AlumnoDAO alumnodao=new AlumnoDAO();
		List<com.domain.modelo.Model> alumnos = new ArrayList<com.domain.modelo.Model>();
		List<com.domain.modelo.Model> listaAlumnos=new ArrayList<com.domain.modelo.Model>();
		int i=1;
		do {
			
			try {
				alumnos = alumnodao.leer(new Alumno(i));
				if(!alumnos.isEmpty())
					listaAlumnos.add(((Alumno) alumnos.get(0)));
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}while(i<=19);
		

		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos", listaAlumnos);


		return "Listado";
	}
}
