package modulo9_practicasResueltas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import apuntesclase.notas.modelo.Model;
import apuntesclase.notas.modelo.PracticaResuelta;
import apuntesclase.notas.modelo.dao.DAO;
import apuntesclase.notas.util.ConnectionManager;

public class PracticaResueltaDAO implements DAO{
	private Connection conexion;
	public PracticaResueltaDAO() {

	}
	@Override
	public void agregar(Model pMod) throws ClassNotFoundException, SQLException {
		PracticaResuelta practica= (PracticaResuelta)pMod;

		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();

		StringBuilder sql = new StringBuilder("INSERT INTO practicasresueltas (prac_id, alu_id, pr_nota, pr_observaciones) VALUES");
		sql.append("(?,?,?,?)");

		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setInt(1, practica.getCodPrac());
		pstm.setInt(2, practica.getCodAlu());
		pstm.setFloat(3, practica.getNota());
		pstm.setString(4, practica.getObservaciones());

		pstm.executeUpdate();
		ConnectionManager.desConectar();
		
	}

	@Override
	public void modificar(Model pMod) throws ClassNotFoundException, SQLException {
		PracticaResuelta practica= (PracticaResuelta)pMod;

		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();

		StringBuilder sql = new StringBuilder("update practicasresueltas ");
		sql.append("set prac_id=?, alu_id=?, pr_nota=?, pr_observaciones=? ");
		sql.append("where pr_id=?");

		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setInt(1, practica.getCodPrac());
		pstm.setInt(2, practica.getCodAlu());
		pstm.setFloat(3, practica.getNota());
		pstm.setString(4, practica.getObservaciones());
		pstm.setInt(5, practica.getCodigo());

		pstm.executeUpdate();
		ConnectionManager.desConectar();
		
	}

	@Override
	public void eliminar(Model pMod) throws ClassNotFoundException, SQLException {
		PracticaResuelta practica= (PracticaResuelta)pMod;

		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();

		StringBuilder sql = new StringBuilder("delete from practicasresueltas");
		sql.append("where pr_id=?");

		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setInt(1, practica.getCodigo());
		

		pstm.executeUpdate();
		ConnectionManager.desConectar();
		
	}

	@Override
	public List<Model> leer(Model pMod) throws ClassNotFoundException, SQLException {
		List<Model> practicasRes=new ArrayList<Model>();
		PracticaResuelta practicaRes=(PracticaResuelta)pMod;
		
		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();
		
		StringBuilder sql = new StringBuilder("select pr_id, prac_id, alu_id, pr_nota, pr_observaciones ");
		sql.append("from practicasresueltas ");
		sql.append("where pr_id=? ");
		
		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setInt(1, practicaRes.getCodigo());
		
		ResultSet rs=pstm.executeQuery();
		
		while(rs.next()) {
			practicasRes.add(new PracticaResuelta(  rs.getInt("pr_id"),
													rs.getInt("prac_id"),
													rs.getInt("alu_id"),
													rs.getInt("pr_nota"),
													rs.getString("pr_observaciones")));
		}
		rs.close();
		ConnectionManager.desConectar();
		
		return practicasRes;
		
	}

}
