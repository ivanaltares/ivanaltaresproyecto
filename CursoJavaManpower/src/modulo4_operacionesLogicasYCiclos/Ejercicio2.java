package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.println("Introducir el numero: ");
		int num=sc.nextInt();

		if(num%2==0)
			System.out.println("El numero introducido es par. ");
		else
			System.out.println("El numero introducido es impar. ");
	}

}
