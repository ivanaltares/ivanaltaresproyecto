package modulo2_variables;

public class Mod2_Ejercicio2 {

	//ejercicio para practicar el rango de distintos tipos de variables primitivas
	public static void main(String[] args) {

		byte byteMin=(byte) -(Math.pow(2, 7));
		byte byteMax=(byte) (Math.pow(2, 7)-1);
		short shortMin=(short) -(Math.pow(2, 15));
		short shortMax=(short) (Math.pow(2, 15)-1);
		int intMin=(int) -(Math.pow(2, 31));
		int intMax=(int) (Math.pow(2, 31)-1);
		long longMin=(long) -(Math.pow(2, 63));
		long longMax=(long) (Math.pow(2, 63)-1);

		System.out.println("tipo\t\t\tminimo\t\t\tmaximo");
		System.out.println("....\t\t\t......\t\t\t......");
		System.out.println("\nbyte\t\t\t"+byteMin+"\t\t\t"+byteMax);
		System.out.println("\nshort\t\t\t"+shortMin+"\t\t\t"+shortMax);
		System.out.println("\nint\t\t\t"+intMin+"\t\t"+intMax);
		System.out.println("\nlong\t\t\t"+longMin+"\t"+longMax);

		System.out.println("\nLa formula general para sacar los minimos y maximos de una variable es: \n");
		System.out.println("M�nimo\t\tM�ximo");
		System.out.println("-(2^(bits-1))\t2^(bits-1)-1");
	}

}
