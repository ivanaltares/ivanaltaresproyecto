package com.ivanCode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ivanCode.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer> {

	Usuario findByNombre(String username);

}
