package modulo2_variables;

public class Mod2_Ejercicio3 {

	public static void main(String[] args) {

		//ejercicio para practicar el uso de memoria optimo al almacenar datos

		char division='a';
		byte goles=10;
		int capacidadTotal=70000;
		//asumo que el promedio de goles es la suma de los goles de una temporada o equipo
		short promedioGoles=200;

		System.out.println("Para una letra el tipo de variable char ser� suficiente. \nDivision: "+division);
		System.out.println("Para los goles de un partido en los que raramente superar�n 10 nos vale con 127 de maximo (byte). \nGoles: "+goles);
		System.out.println("Para la capacidad total de hinchas necesitaremos mas de 32000, por lo tanto usaremos int. \nCapacidad: "+capacidadTotal);
		System.out.println("Y para el promedio de goles con un maximo de 32000 no tendremos problemas (short). \nPromedio: "+promedioGoles);

	}

}
