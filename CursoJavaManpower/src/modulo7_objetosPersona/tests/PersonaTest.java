package modulo7_objetosPersona.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modulo7_objetosPersona.Alumno;
import modulo7_objetosPersona.Persona;
import modulo7_objetosPersona.Profesor;

class PersonaTest {

	Persona pVacia;
	Persona pLlena;

	Alumno aVacio;
	Alumno aLleno;

	Profesor pVacio;
	Profesor pLleno;

	List<Persona> listaPersonas;
	Set<Persona> setPersonas;

	@BeforeEach
	void setUp() throws Exception {
		pVacia=new Persona();
		pLlena=new Persona("Ivan", "Altares");

		aVacio=new Alumno();
		aLleno=new Alumno("Ivan", "Altares", 1);

		pVacio=new Profesor();
		pLleno=new Profesor("Ivan", "Altares", "AAA");

		listaPersonas=new ArrayList<>();
		listaPersonas.add(pLlena);
		listaPersonas.add(aLleno);

		setPersonas=new HashSet<>();
		setPersonas.add(pLlena);
		setPersonas.add(aLleno);

	}

	@AfterEach
	void tearDown() throws Exception {
		pVacia=null;
		pLlena=null;

		aVacio=null;
		aLleno=null;

		pVacio=null;
		pLleno=null;

		listaPersonas=null;
		setPersonas=null;
	}

	@Test
	void testPersonaVacia() {
		assertEquals(pVacia.getNombre(), "");
		assertEquals(pVacia.getApellido(), "");
	}

	@Test
	void testPersonaLlena() {
		assertEquals(pLlena.getNombre(), "Ivan");
		assertEquals(pLlena.getApellido(), "Altares");
	}

	@Test
	void testAlumnoVacio() {
		assertEquals(aVacio.getNombre(), "");
		assertEquals(aVacio.getApellido(), "");
		assertEquals(aVacio.getLegajo(), 0);
	}

	@Test
	void testAlumnoLleno() {
		assertEquals(aLleno.getNombre(), "Ivan");
		assertEquals(aLleno.getApellido(), "Altares");
		assertEquals(aLleno.getLegajo(), 1);
	}

	@Test
	void testProfesorVacio() {
		assertEquals(pVacio.getNombre(), "");
		assertEquals(pVacio.getApellido(), "");
		assertEquals(pVacio.getIosfa(), "");
	}

	@Test
	void testProfesorLleno() {
		assertEquals(pLleno.getNombre(), "Ivan");
		assertEquals(pLleno.getApellido(), "Altares");
		assertEquals(pLleno.getIosfa(), "AAA");
	}

	@Test
	void testPersonaEqualsTrue() {
		Persona p=new Persona("Ivan", "Altares");
		assertTrue(pLlena.equals(p));
	}

	@Test
	void testPersonaEqualsFalse() {
		Persona p=new Persona("Pepe", "Altares");
		assertFalse(pLlena.equals(p));
	}

	@Test
	void testAlumnoEqualsTrue() {
		Alumno a=new Alumno("Ivan", "Altares", 1);
		assertTrue(aLleno.equals(a));
	}

	@Test
	void testAlumnoEqualsFalse() {
		Alumno a=new Alumno("Pepe", "Altares", 4);
		assertFalse(aLleno.equals(a));
	}

	@Test
	void testProfesorEqualsTrue() {
		Profesor p=new Profesor("Ivan", "Altares", "AAA");
		assertTrue(pLleno.equals(p));
	}

	@Test
	void testProfesorEqualsFalse() {
		Profesor p=new Profesor("Pepe", "Altares", "BBB");
		assertFalse(pLleno.equals(p));
	}

	@Test
	public void listaEqualsContainsTRUE(){
		Alumno a=new Alumno("Ivan", "Altares", 1);
		assertTrue(listaPersonas.contains(a));
	}

	@Test
	public void listaEqualsContainsFALSE(){
		Alumno a=new Alumno("Pepe", "Rodriguez", 1);
		assertFalse(listaPersonas.contains(a));
	}
	
	@Test
	public void testListaAdd(){		
		Persona pers = new Persona("Pepe", "Rodriguez");		
		assertTrue(listaPersonas.add(pers));
	}
	@Test
	public void testSetAdd(){
		Persona pers = new Persona("Pepe", "Rodriguez");		
		assertTrue(setPersonas.add(pers));
	}

}
