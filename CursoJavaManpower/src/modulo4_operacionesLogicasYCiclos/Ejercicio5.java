package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir el puesto final del participante: ");
		int puesto = sc.nextInt();

		if(puesto==1)
			System.out.println("�Obtiene la medalla de oro! ");
		else if(puesto==2)
			System.out.println("�Obtiene la medalla de plata! ");
		else if(puesto==3)
			System.out.println("�Obtiene la medalla de bronce! ");
		else
			System.out.println("Siga participando. ");
	}

}
