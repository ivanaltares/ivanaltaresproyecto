package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir el numero: ");
		int num = sc.nextInt();

		for(int i=1; i<11; i++)
			System.out.println(num+" \tx \t"+i+" \t= \t"+num*i);


	}

}
