package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio13 {
	//nota: ya realice el ejercicio 3 con un switch sin saber que habia un ejercicio que te lo pedia de una manera especifica,
	//asi que he copiado el ejercicio 3 en el 13
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir el numero del mes: ");
		int mes = sc.nextInt();

		switch(mes) {
			case 1:
				System.out.println("Enero tiene 31 dias. ");
				break;
			case 2:
				System.out.println("Febrero tiene 28 dias. ");
				break;
			case 3:
				System.out.println("Marzo tiene 31 dias. ");
				break;
			case 4:
				System.out.println("Abril tiene 30 dias. ");
				break;
			case 5:
				System.out.println("Mayo tiene 31 dias. ");
				break;
			case 6:
				System.out.println("Junio tiene 30 dias. ");
				break;
			case 7:
				System.out.println("Julio tiene 31 dias. ");
				break;
			case 8:
				System.out.println("Agosto tiene 31 dias. ");
				break;
			case 9:
				System.out.println("Septiembre tiene 30 dias. ");
				break;
			case 10:
				System.out.println("Octubre tiene 31 dias. ");
				break;
			case 11:
				System.out.println("Noviembre tiene 30 dias. ");
				break;
			case 12:
				System.out.println("Diciembre tiene 31 dias. ");
				break;

		}
	}
}
