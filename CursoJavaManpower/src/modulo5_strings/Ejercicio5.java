package modulo5_strings;

public class Ejercicio5 {

	public static void main(String[] args) {

		String var="gcasas1972@gmail.com";

		//ejercicio para practicar substrings

		int posicion=var.indexOf('@');
		String subcadena1=var.substring(0, posicion);
		String subcadena2=var.substring(posicion+1, var.length());

		System.out.println("Posicion de la @ empezando de 0: "+posicion);
		System.out.println("Parte 1: "+subcadena1);
		System.out.println("Parte 2: "+subcadena2);
	}

}
