package apuntesclase.notas.modelo.dao.tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import apuntesclase.notas.modelo.Alumno;
import apuntesclase.notas.modelo.Model;
import apuntesclase.notas.modelo.dao.AlumnoDAO;
import apuntesclase.notas.modelo.dao.DAO;
import apuntesclase.notas.util.ConnectionManager;

public class AlumnoDAOTest {
	DAO alumnoDao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( AlumnoDAOTest.class.getResource( "AlumnosCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desConectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( AlumnoDAOTest.class.getResource( "AlumnosEliminar.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desConectar();
	}

	@Before
	public void setUp() throws Exception {
		alumnoDao = new AlumnoDAO();
	}

	@After
	public void tearDown() throws Exception {
		alumnoDao = null;
	}

	@Test
	public void testAgregar() {
		try {
			alumnoDao.agregar(new Alumno(0, "Gabriel_test", "Casas_test", "Estudios_test", "Repo_test"));
			//tengo que leer 
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select alu_nombre from alumnos where alu_nombre ='Gabriel_test'");
			rs.next();
			assertEquals("Gabriel_test", rs.getString("alu_nombre"));
			
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testModificar() {
		try {
 
			//1� leer los datos del alumno a cambiar
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm=con.createStatement();
			ResultSet rs=stm.executeQuery("select alu_id from alumnos where alu_nombre='Marina_test'");
			rs.next();
			
			//2� modificar el objeto con los datos del alumno
			Alumno a=new Alumno(	rs.getInt("alu_id"), 
									"Nombre_Modificado", 
									"Apellido_Modificado", 
									"Estudios_Modificado", 
									"Repo");
			alumnoDao.modificar(a);
			
			//3� leer el resultado
			StringBuilder sql=new StringBuilder("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit ");
			sql.append("from alumnos ");
			sql.append("where alu_nombre='Nombre_Modificado' ");
			
			rs=stm.executeQuery(sql.toString());
			rs.next();
			Alumno alumnoLeido=new Alumno(	rs.getInt("alu_id"),
											rs.getString("alu_nombre"), 
											rs.getString("alu_apellido"), 
											rs.getString("alu_estudios"), 
											rs.getString("alu_linkgit"));
			
			assertEquals(a.getCodigo(), alumnoLeido.getCodigo());
			assertEquals(a.getNombre(), alumnoLeido.getNombre());
			assertEquals(a.getApellido(), alumnoLeido.getApellido());
			assertEquals(a.getEstudios(), alumnoLeido.getEstudios());
			assertEquals(a.getLinkArepositorio(), alumnoLeido.getLinkArepositorio());
			
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testEliminar() {
		try {
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select  ALU_ID  from alumnos where alu_nombre ='Monsef_test'");
			rs.next();

			// 2- elimino
			Alumno alu = new Alumno(rs.getInt("ALU_ID"));
			alumnoDao.eliminar(alu);
			// 3- que paso?

			rs = stm.executeQuery("Select  ALU_ID  from alumnos where alu_nombre ='Monsef_test'");
			assertFalse(rs.next());

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testLeer() {
		try {
			// 1- lectura de los datos objetivo conseguir el id
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select  ALU_ID  from alumnos where alu_nombre ='Aar�n_test'");
			rs.next();

			Alumno alu = new Alumno(rs.getInt("ALU_ID"));
			List<Model> alumnos = alumnoDao.leer(alu);

			assertEquals("Aar�n_test", ((Alumno) alumnos.get(0)).getNombre());
			assertEquals("S�nchez S�nchez_test", ((Alumno) alumnos.get(0)).getApellido());
			assertEquals("Desarrollo de Aplicaciones Multiplataforma_test", ((Alumno) alumnos.get(0)).getEstudios());
			assertEquals("https://github.com/Pashinian/CursoJava2021.git_test",
					((Alumno) alumnos.get(0)).getLinkArepositorio());

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

}
