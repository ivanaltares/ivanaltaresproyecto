package modulo5_strings;

public class Ejercicio1 {


	public static void main(String[] args) {

		String var="Hola Mundo";

		//mostrando texto todo en mayusculas
		String var2=var.toUpperCase();
		System.out.println("Texto en mayusculas:\n"+var2);

		//mostrando texto todo en minusculas
		String var3=var.toLowerCase();
		System.out.println("Texto en minusculas:\n"+var3);

		//mostrando o como 0
		String var4=var.replace('o', '0');
		System.out.println("Texto con 0:\n"+var4);
	}

}
