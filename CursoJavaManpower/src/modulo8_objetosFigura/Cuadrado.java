package modulo8_objetosFigura;

import java.util.Objects;

public class Cuadrado extends Figura {

	private float lado;
	
	public Cuadrado(String nombre, float lado) {
		super(nombre);
		this.lado = lado;
	}

	public Cuadrado() {
		super();
		lado=0;
	}


	@Override
	public float calcularPerimetro() {
		return 4*lado;
	}

	@Override
	public float calcularSuperficie() {
		float sup=(float) (lado*lado);
		if(sup>Figura.maximaSuperficie)
			Figura.setMaximaSuperficie(sup);
		return sup;
	}

	@Override
	public String getValores() {
		return "lado="+lado;
	}

	public float getLado() {
		return lado;
	}

	public void setLado(float lado) {
		this.lado = lado;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(lado);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Cuadrado 	&&
				super.equals(obj)		&&
				((Cuadrado)obj).getLado()==lado;
	}

	@Override
	public String toString() {
		return "lado=" + lado;
	}

}
