package modulo8_objetosFigura;

import java.util.Objects;

import modulo7_objetosPersona.Persona;

public class Circulo extends Figura {

	private float radio;
	
	public Circulo() {
		super();
		radio=0;
	}

	public Circulo(String nombre, float radio) {
		super(nombre);
		this.radio=radio;
	}

	@Override
	public float calcularPerimetro() {
		return (float) (2*Math.PI*radio);
	}

	@Override
	public float calcularSuperficie() {
		float sup=(float) (Math.PI*radio*radio);
		if(sup>Figura.maximaSuperficie)
			Figura.setMaximaSuperficie(sup);
		return sup;
	}

	@Override
	public String getValores() {
		return "radio= "+radio ;
	}

	public float getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		this.radio = radio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(radio);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Circulo 	&&
				super.equals(obj)		&&
				((Circulo)obj).getRadio()==radio;
	}

	@Override
	public String toString() {
		return "radio=" + radio;
	}
	
	

}
