package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir la categoria (a, b, c): ");
		char cat = sc.nextLine().charAt(0);

		if(cat=='a')
			System.out.println("Hijo. ");
		else if(cat=='b')
			System.out.println("Padres. ");
		else if(cat=='c')
			System.out.println("Abuelos. ");
		else
			System.out.println("No se ha introducido a, b � c. ");
	}

}
