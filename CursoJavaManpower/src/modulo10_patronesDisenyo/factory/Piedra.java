package modulo10_patronesDisenyo.factory;

public class Piedra extends PiedraPapelTijeraFactory {

	public Piedra() {
		numero = 0;
		nombre = "piedra";
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == 0;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pObj) {
		// papel pierde -1
		// tijera gana 1
		// piedra empata 0

		/*
		 * -1 derrota 0 empata 1 gana
		 */
		int resul = 0;

		switch (pObj.numero) {
		case 0:
			resul = 0;
			break;
		case 1:
			resul = -1;
			break;
		case 2:
			resul = 1;
			break;
		default:
			break;
		}

		return resul;
	}

}
