package com.domain.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.domain.modelo.Alumno;
import com.domain.modelo.Model;
import com.domain.util.ConnectionManager;

public class AlumnoDAO implements DAO {
	private Connection conexion;
	public AlumnoDAO() {

	}

	public void agregar(Model pMod) throws ClassNotFoundException, SQLException {
		Alumno alumno= (Alumno)pMod;

		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();

		StringBuilder sql = new StringBuilder("INSERT INTO alumnos (ALU_NOMBRE,ALU_APELLIDO,ALU_ESTUDIOS,ALU_LINKGIT) VALUES");
		sql.append("(?,?,?,?)");

		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setString(1, alumno.getNombre());
		pstm.setString(2, alumno.getApellido());
		pstm.setString(3, alumno.getEstudios());
		pstm.setString(4, alumno.getLinkArepositorio());

		pstm.executeUpdate();
		ConnectionManager.desConectar();
	}

	public void modificar(Model pMod) throws ClassNotFoundException, SQLException {

		Alumno alumno= (Alumno)pMod;

		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();

		StringBuilder sql = new StringBuilder("update alumnos set alu_nombre=?, alu_apellido=?, alu_estudios=?, alu_linkgit=? where alu_id=?");

		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setString(1, alumno.getNombre());
		pstm.setString(2, alumno.getApellido());
		pstm.setString(3, alumno.getEstudios());
		pstm.setString(4, alumno.getLinkArepositorio());
		pstm.setInt(5, alumno.getCodigo());

		pstm.executeUpdate();

		ConnectionManager.desConectar();
	}

	public void eliminar(Model pMod) throws ClassNotFoundException, SQLException {
		Alumno alumno= (Alumno)pMod;

		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();

		StringBuilder sql = new StringBuilder("delete from alumnos where alu_id= ?");
		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setInt(1, alumno.getCodigo());

		pstm.executeUpdate();

		ConnectionManager.desConectar();
	}

	public List<Model> leer(Model pMod) throws ClassNotFoundException, SQLException {
		List<Model> alumnos=new ArrayList<Model>();
		Alumno alumno= (Alumno)pMod;

		ConnectionManager.conectar();
		conexion = ConnectionManager.getConection();

		StringBuilder sql = new StringBuilder("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit ");
		sql.append("from alumnos ");
		sql.append("where alu_id=? ");

		PreparedStatement pstm = conexion.prepareStatement(sql.toString());
		pstm.setInt(1, alumno.getCodigo());

		ResultSet rs=pstm.executeQuery();

		while(rs.next()) {
			alumnos.add(new Alumno( rs.getInt("alu_id"),
									rs.getString("alu_nombre"),
									rs.getString("alu_apellido"),
									rs.getString("alu_estudios"),
									rs.getString("alu_linkgit")));
		}

		rs.close();
		ConnectionManager.desConectar();

		return alumnos;
	}



}
