package modulo5_strings;

public class Ejercicio4 {

	public static void main(String[] args) {

		String var="esto es una prueba de la clase String";
		int sumaVocal=0;
		int sumaCons=0;

		//ejercicio para determinar n de vocales y consonantes
		for(int i=0; i<var.length();i++) {
			if(var.charAt(i)=='a' || var.charAt(i)=='e' ||var.charAt(i)=='i' ||var.charAt(i)=='o' ||var.charAt(i)=='u' )
				sumaVocal++;
			else if(var.charAt(i)==' ');
			else
				sumaCons++;
		}

		System.out.println("Texto: "+var);
		System.out.println("Numero de vocales: "+sumaVocal);
		System.out.println("Numero de consonantes: "+sumaCons);
	}

}
