package modulo8_objetosFigura;

import java.util.Objects;

public class Rectangulo extends Figura{

	private float altura, base;
	
	public Rectangulo() {
		super();
		altura=0;
		base=0;
	}
	
	public Rectangulo(String nombre) {
		super(nombre);
		altura=0;
		base=0;
	}
	
	public Rectangulo(String nombre, float altura, float base) {
		super(nombre);
		this.altura=altura;
		this.base=base;
	}

	@Override
	public float calcularPerimetro() {
		return altura+altura+base+base;
	}

	@Override
	public float calcularSuperficie() {
		float sup=(float) (altura*base);
		if(sup>Figura.maximaSuperficie)
			Figura.setMaximaSuperficie(sup);
		return sup;
	}

	@Override
	public String getValores() {
		return "altura= "+altura+", base="+base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(altura, base);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Rectangulo 			&&
				super.equals(obj)					&&
				((Rectangulo)obj).getBase()==base	&& 
				((Rectangulo)obj).getAltura()==altura;
	}

	@Override
	public String toString() {
		return "Rectangulo: altura=" + altura + ", base=" + base;
	}
	
	
	

}
