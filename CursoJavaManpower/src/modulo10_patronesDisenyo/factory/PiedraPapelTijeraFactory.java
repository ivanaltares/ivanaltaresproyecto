package modulo10_patronesDisenyo.factory;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {

	private static List<PiedraPapelTijeraFactory> elementos;
	protected String nombre;
	protected int numero;
	
	public PiedraPapelTijeraFactory() {
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public static PiedraPapelTijeraFactory getInstance(int pNum) {
		//el padre conoce a todos sus hijos
		elementos=new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Tijera());
		elementos.add(new Papel());
		
		for(PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			if(piedraPapelTijeraFactory.isMe(pNum))
				return piedraPapelTijeraFactory;
		}
		return null;
	}
	
	public abstract boolean isMe(int pNum);
	public abstract int comparar(PiedraPapelTijeraFactory pObj);
}
