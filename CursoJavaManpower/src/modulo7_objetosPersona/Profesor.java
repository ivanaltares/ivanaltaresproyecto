package modulo7_objetosPersona;

import java.util.Objects;

public class Profesor extends Persona{

	private String iosfa;

	public Profesor() {
		super();
		iosfa="";
	}

	public Profesor(String iosfa) {
		super();
		this.iosfa=iosfa;
	}

	public Profesor(String nombre, String apellido, String iosfa) {
		super(nombre, apellido);
		this.iosfa=iosfa;
	}

	public String getIosfa() {
		return iosfa;
	}

	public void setIosfa(String iosfa) {
		this.iosfa = iosfa;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.getApellido(),super.getNombre(),iosfa);
	}

	@Override
	public boolean equals(Object obj) {

		return obj instanceof Profesor && super.equals(obj) && this.iosfa.equals(((Profesor)obj).getIosfa());
	}

	@Override
	public String toString() {
		return "Profesor:\tApellido=" + super.getApellido() + ",\tNombre=" + super.getNombre() + ",\tIOSFA="+iosfa+". ";
	}


}
