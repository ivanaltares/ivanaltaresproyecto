package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio8 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir variable 1 (0-piedra, 1-papel, 2-tijera): ");
		int num1 = sc.nextInt();
		System.out.println("Introducir variable 2 (0-piedra, 1-papel, 2-tijera): ");
		int num2 = sc.nextInt();

		if((num1==0 && num2==2)||(num1==1 && num2==0)||(num1==2 && num2==1))
			System.out.println("Jugador 1 gana. ");
		else if(num1==num2)
			System.out.println("Empate. ");
		else if((num2==0 && num1==2)||(num2==1 && num1==0)||(num2==2 && num1==1))
			System.out.println("Jugador 2 gana. ");
		else
			System.out.println("Error en los datos. ");

	}

}
