package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir el auto (a, b, c): ");
		char coche = sc.nextLine().charAt(0);

		switch(coche) {
			case 'a':
				System.out.println("El coche 'a' tiene 4 ruedas y un motor. ");
				break;
			case 'b':
				System.out.println("El coche 'b' tiene 4 ruedas, un motor, cierre centralizado y aire. ");
				break;
			case 'c':
				System.out.println("El coche 'c' tiene 4 ruedas, un motor, cierre centralizado, aire y airbag. ");
				break;
			default:
				System.out.println("Coche introducido incorrecto. ");
				break;
		}
	}

}
