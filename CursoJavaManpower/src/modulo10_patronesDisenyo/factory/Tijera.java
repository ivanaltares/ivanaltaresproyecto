package modulo10_patronesDisenyo.factory;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera() {
		numero = 2;
		nombre = "tijera";
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == 2;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pObj) {
		// piedra pierde -1
		// papel gana 1
		// tijera empata 0

		/*
		 * -1 derrota 0 empata 1 gana
		 */
		int resul = 0;

		switch (pObj.numero) {
		case 0:
			resul = -1;
			break;
		case 1:
			resul = 1;
			break;
		case 2:
			resul = 0;
			break;
		default:
			break;
		}

		return resul;
	}

}
