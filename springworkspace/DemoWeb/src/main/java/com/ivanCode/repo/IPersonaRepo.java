package com.ivanCode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ivanCode.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

}
