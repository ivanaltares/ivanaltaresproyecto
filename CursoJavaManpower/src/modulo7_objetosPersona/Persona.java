package modulo7_objetosPersona;

import java.util.Objects;

public class Persona {

	private String apellido, nombre;

	public Persona() {
		this.apellido = "";
		this.nombre = "";
	}

	public Persona(String nombre, String apellido) {
		this.apellido = apellido;
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombre, apellido);
	}

	@Override
	public boolean equals(Object obj) {

		return obj instanceof Persona 							&& 
				this.nombre.equals(((Persona) obj).getNombre())	&& 
				this.apellido.equals(((Persona) obj).getApellido());
	}

	@Override
	public String toString() {
		return "Persona:\tApellido=" + apellido + ",\tnombre=" + nombre + ". ";
	}

}
