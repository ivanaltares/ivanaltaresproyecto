package modulo10_patronesDisenyo.factory;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		numero = 1;
		nombre = "papel";
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum == 1;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pObj) {
		// tijera pierde -1
		// piedra gana 1
		// papel empata 0

		/*
		 * -1 derrota 0 empata 1 gana
		 */
		int resul = 0;

		switch (pObj.numero) {
		case 0:
			resul = 1;
			break;
		case 1:
			resul = 0;
			break;
		case 2:
			resul = -1;
			break;
		default:
			break;
		}

		return resul;
	}

}
