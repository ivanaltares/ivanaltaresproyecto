package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introducir el puesto final del participante: ");
		int puesto = sc.nextInt();

		switch(puesto) {
			case 1:
				System.out.println("�Obtiene la medalla de oro! ");
				break;
			case 2:
				System.out.println("�Obtiene la medalla de plata! ");
				break;
			case 3:
				System.out.println("�Obtiene la medalla de bronce! ");
				break;
			default:
				System.out.println("Siga participando. ");
				break;
		}
	}

}
