package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.println("Primero introduzca la categoria (a, b o c): ");
		char categoria=sc.nextLine().charAt(0);
		System.out.println("A continuacion la antiguedad: ");
		int ant=sc.nextInt();
		System.out.println("Por ultimo el sueldo bruto: ");
		float sueldo=sc.nextFloat();

		float sueldoTotal=0;

		if(ant>=1 && ant<=5)
			sueldoTotal=sueldo*1.05f;
		else if(ant>=6 && ant<=10)
			sueldoTotal=sueldo*1.10f;
		else if(ant>10)
			sueldoTotal=sueldo*1.30f;
		else
			System.out.println("Antiguedad erronea. ");

		if(categoria=='a')
			sueldoTotal+=1000;
		else if(categoria=='b')
			sueldoTotal+=2000;
		else if(categoria=='c')
			sueldoTotal+=3000;
		else
			System.out.println("Categoria erronea. ");

		System.out.println("\nSueldo neto final: "+sueldoTotal);


	}

}
