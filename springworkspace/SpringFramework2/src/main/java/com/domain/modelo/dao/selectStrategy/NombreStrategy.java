package com.domain.modelo.dao.selectStrategy;

public class NombreStrategy extends SelectStrategy {

	public NombreStrategy() {

	}

	@Override
	public String getCondicion() {
		StringBuilder sb = new StringBuilder(" where alu_nombre=");
		sb.append(alumno.getNombre());
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		tengoWhere =(alumno.getNombre()!=null && !alumno.getNombre().isEmpty());
		return (alumno.getNombre()!=null && !alumno.getNombre().isEmpty());
	}

}
