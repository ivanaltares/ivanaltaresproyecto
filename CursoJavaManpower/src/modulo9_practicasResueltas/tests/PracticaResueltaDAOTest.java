package modulo9_practicasResueltas.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import apuntesclase.notas.modelo.Model;
import apuntesclase.notas.modelo.PracticaResuelta;
import apuntesclase.notas.modelo.dao.DAO;
import apuntesclase.notas.util.ConnectionManager;
import modulo9_practicasResueltas.PracticaResueltaDAO;

class PracticaResueltaDAOTest {

	DAO practicaResDao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( PracticaResueltaDAOTest.class.getResource( "PracticasResCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desConectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConection();
		
	    Statement consulta= con.createStatement();
	
	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( PracticaResueltaDAOTest.class.getResource( "PracticasResEliminar.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql );
	       }
	    }
	    ConnectionManager.desConectar();
	}

	@BeforeEach
	public void setUp() throws Exception {
		practicaResDao = new PracticaResueltaDAO();
	}

	@AfterEach
	public void tearDown() throws Exception {
		practicaResDao = null;
	}
	
	@Test
	public void testAgregar() {
		try {
			practicaResDao.agregar(new PracticaResuelta(0, 15, 15, 15, "anyadida "));
			//tengo que leer 
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select pr_observaciones from practicasresueltas where pr_observaciones ='anyadida' ");
			rs.next();
			assertEquals("anyadida", rs.getString("pr_observaciones"));
			
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testModificar() {
		try {
 
			//1� leer los datos de las practicas resueltas a cambiar
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm=con.createStatement();
			ResultSet rs=stm.executeQuery("select pr_observaciones from practicasresueltas where pr_observaciones='no tan bien_para modificar' ");
			rs.next();
			
			//2� modificar el objeto con los datos de la practica resuelta
			PracticaResuelta p=new PracticaResuelta(	rs.getInt("pr_id"), 
									2, 
									16, 
									8, 
									"modificao");
			practicaResDao.modificar(p);
			
			//3� leer el resultado
			StringBuilder sql=new StringBuilder("select pr_id, prac_id, alu_id, pr_nota, pr_observaciones ");
			sql.append("from practicasresueltas ");
			sql.append("where pr_observaciones='modificao' ");
			
			rs=stm.executeQuery(sql.toString());
			rs.next();
			PracticaResuelta pracLeida=new PracticaResuelta(rs.getInt("pr_id"),
															rs.getInt("prac_id"), 
															rs.getInt("alu_id"), 
															rs.getInt("pr_nota"), 
															rs.getString("pr_observaciones"));
			
			assertEquals(p.getCodigo(), pracLeida.getCodigo());
			assertEquals(p.getCodPrac(), pracLeida.getCodPrac());
			assertEquals(p.getCodAlu(), pracLeida.getCodAlu());
			assertEquals(p.getNota(), pracLeida.getNota());
			assertEquals(p.getObservaciones(), pracLeida.getObservaciones());
			
			
		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testEliminar() {
		try {
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select pr_observaciones from practicasresueltas where pr_observaciones ='perfectisima_para borrar'");
			rs.next();

			// 2- elimino
			PracticaResuelta p = new PracticaResuelta();
			practicaResDao.eliminar(p);
			// 3- que paso?

			rs = stm.executeQuery("Select  pr_observaciones  from practicasresueltas where pr_observaciones ='perfectisima_para borrar'");
			assertFalse(rs.next());

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testLeer() {
		try {
			// 1- lectura de los datos objetivo conseguir el id
			ConnectionManager.conectar();
			Connection con = ConnectionManager.getConection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select  pr_observaciones  from practicasresueltas where pr_observaciones ='aprobao_para leer'");
			rs.next();

			PracticaResuelta p = new PracticaResuelta(rs.getInt("pr_id"));
			List<Model> practicasRes = practicaResDao.leer(p);

			assertEquals(3, ((PracticaResuelta) practicasRes.get(0)).getCodPrac());
			assertEquals(2, ((PracticaResuelta) practicasRes.get(0)).getCodAlu());
			assertEquals(5, ((PracticaResuelta) practicasRes.get(0)).getNota());
			assertEquals("aprobao_para leer", ((PracticaResuelta) practicasRes.get(0)).getObservaciones());

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

}
