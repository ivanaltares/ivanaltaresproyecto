<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@page import="java.util.List"%>
    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${titulo}</title>
	<h1>PROFESOR ${profesor}</h1>
	</br>
</head>
<body>
	<ul>
		<c:forEach items="${alumnos}" var="alumno">
			<li> <c:out value="${alumno.getCodigo()}" /> </li>
			<li> <c:out value="${alumno.getNombre()}" /> </li>
			<li> <c:out value="${alumno.getApellido()}" /> </li>
		</c:forEach>
	</ul>
	
</body>
</html>