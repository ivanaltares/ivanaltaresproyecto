package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		for(int j=2; j<11; j++) {
			for(int i=1; i<11; i++)
				System.out.println(j+" \tx \t"+i+" \t= \t"+j*i);
			System.out.println();
		}
	}

}
