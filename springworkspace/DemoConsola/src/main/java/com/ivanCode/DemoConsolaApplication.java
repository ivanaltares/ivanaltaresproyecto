package com.ivanCode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ivanCode.service.IpersonaService;
import com.ivanCode.service.PersonaServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class DemoConsolaApplication implements CommandLineRunner{
	
	private static Logger log=LoggerFactory.getLogger(DemoConsolaApplication.class);
	@Autowired
	private IpersonaService service;
	
	public static void main(String[] args) {
		SpringApplication.run(DemoConsolaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("impresion en consola");
		log.warn("esto es una advertencia");
		log.error("esto es un error");
		
		//service=new PersonaServiceImpl();
		service.registrarHandler("Ivan");
	}

}
