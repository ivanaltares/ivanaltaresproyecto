package modulo5_strings;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		//ejercicio que dados 2 strings los concatena con un espacio en medio
		System.out.println("Introduzca nombre: ");
		String nom=sc.nextLine();
		System.out.println("Introduzca apellido: ");
		String ape=sc.nextLine();

		String sfinal=nom+" "+ape;
		StringBuilder sb=new StringBuilder(nom);
		sb.append(' ');
		sb.append(ape);
		System.out.println("String final: "+sb);
	}

}
