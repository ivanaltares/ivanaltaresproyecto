package modulo10_patronesDisenyo.Selectstrategy;

public class EstudiosStrategy extends SelectStrategy {

	public EstudiosStrategy() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCondicion() {
		StringBuilder sb = new StringBuilder("");
		if(tengoWhere) {
			sb.append(" and alu_estudios='");
		}
		else {
			sb.append(" where alu_estudios='");
			tengoWhere=true;
		}
			
			
		sb.append(alumno.getEstudios());
		sb.append("'");
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		return (alumno.getEstudios()!=null && !alumno.getEstudios().isEmpty());
	}

}
