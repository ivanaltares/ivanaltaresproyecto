package modulo2_variables;

public class Mod2_Ejercicio4 {

	public static void main(String[] args) {

		// ejercicios para practicar el autocasteo y la operacion entre distintos tipos de datos numericos

		System.out.println("Datos:\n");

		byte b = 10;
		short s = 20;
		int i = 30;
		long l = 40;
		System.out.println("Byte: \t" + b);
		System.out.println("Short: \t" + s);
		System.out.println("Int: \t" + i);
		System.out.println("Long: \t" + l);

		System.out.println("\nEl tipo de dato del resultado de la suma de dos tipos distintos de datos es el tipo que tiene mayor rango.\n");

		byte sumabb = (byte) (b + b);
		short sumabs = (short) (b + s);
		int sumabi = b + i;
		int sumaii = i + i;
		long sumasl = s + l;

		System.out.println("Suma entre 2 bytes: \t\t" + sumabb);
		System.out.println("Suma entre byte y short: \t" + sumabs);
		System.out.println("Suma entre byte y entero: \t" + sumabi);
		System.out.println("Suma entre 2 enteros: \t\t" + sumaii);
		System.out.println("Suma entre short y long: \t" + sumasl);

	}

}
