package modulo5_strings;

public class Ejercicio2 {

	public static void main(String[] args) {

		String var="curso de Java";

		//mostrando la variable anterior separada en varias lineas
		for(int i=0; i<var.length(); i++) {
			if(var.charAt(i)!=' ')
				System.out.print(var.charAt(i));
			else
				System.out.println();
		}
	}

}
