package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio22 {

	public static char calcularCategoria(int categ) {
		switch(categ) {
			case 1:
				return 'a';
			case 2:
				return 'b';
			case 3:
				return 'c';
			default:
				return 'd';
		}
	}

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);

		char categoria;
		int ant=0;
		float sueldo;

		for(int i=0; i<10; i++) {
			System.out.println("Calculando el sueldo del empleado numero "+(i+1));

			categoria=calcularCategoria(1 + (int)(Math.random() * ((3 - 1) + 1)));
			System.out.println("Categoria: "+categoria);
			System.out.println("Introducir la antiguedad: ");
			ant=sc.nextInt();
			sueldo=16000 + (int)(Math.random() * ((20000 - 16000) + 1));
			System.out.println("Sueldo bruto: "+sueldo);


			float sueldoTotal=0;

			if(ant>=1 && ant<=5)
				sueldoTotal=sueldo*1.05f;
			else if(ant>=6 && ant<=10)
				sueldoTotal=sueldo*1.10f;
			else if(ant>10)
				sueldoTotal=sueldo*1.30f;
			else
				System.out.println("Antiguedad erronea. ");

			if(categoria=='a')
				sueldoTotal+=1000;
			else if(categoria=='b')
				sueldoTotal+=2000;
			else if(categoria=='c')
				sueldoTotal+=3000;
			else
				System.out.println("Categoria erronea. ");

			System.out.println("Sueldo neto final: "+sueldoTotal+"\n\n");


		}

		System.out.println("**************************");
		System.out.println("Fin de programa. ");



	}

}
