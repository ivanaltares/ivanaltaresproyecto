package com.domain.modelo.dao.selectStrategy;

public class NullStrategy extends SelectStrategy {

	public NullStrategy() {
		SelectStrategy.isUltimo 	= false;
		SelectStrategy.tengoWhere = false;
	}

	@Override
	public String getCondicion() {
		return "";
	}

	@Override
	public boolean isMe() {
		SelectStrategy.isUltimo = SelectStrategy.alumno == null;
		return SelectStrategy.alumno == null;
	}

}
