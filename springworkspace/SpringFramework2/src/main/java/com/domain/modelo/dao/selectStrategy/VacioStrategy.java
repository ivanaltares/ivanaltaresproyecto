package com.domain.modelo.dao.selectStrategy;

public class VacioStrategy extends SelectStrategy {

	public VacioStrategy() {
		SelectStrategy.isUltimo = false;
		SelectStrategy.tengoWhere =false;
	}

	@Override
	public String getCondicion() {
		return "";
	}

	@Override
	public boolean isMe() {
		SelectStrategy.isUltimo = alumno.isEmpty();
		return alumno.isEmpty();
	}

}
