package conexionbdd_prueba;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ConexionBDD {

	public static void main(String[] args) {

		Connection conn=null;

		//abrir conexion
		try {
			//cargar driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			//abrir conexion
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root", "admin");

		} catch (ClassNotFoundException e) {
			System.out.println("Error abriendo la conexion. ");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Error cargando el driver. ");
			e.printStackTrace();
		}

		//CONSULTAS SQL

		Statement stm;
		try {

			//preparar la query (la statement)
			stm = conn.createStatement();
			//ejecutar la query, devuelve un resultset
			ResultSet resul=stm.executeQuery("select * from alumnos");
			while(resul.next()) {
				System.out.println(resul.getString("alu_nombre")+" - "+resul.getString("alu_apellido"));
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}





		//cerrar conexion
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("Error al cerrar la conexion. ");
			e.printStackTrace();
		}

	}
}
