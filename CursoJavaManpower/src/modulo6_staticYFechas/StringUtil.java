package modulo6_staticYFechas;

public class StringUtil {

	public static boolean containsDobleSpace(String str) {
		int contador=0;
		for(int i=0; i<str.length(); i++)
			if(str.charAt(i)==' ')
				contador++;

		return contador==2;
	}

	//en el enunciado no pone public, asi que asumo que es
	//public al ser un metodo estatico dise�ado para ser usado en otras clases
	public static boolean containsNumber(String str) {
		for(int i=0; i<str.length(); i++) {
			if(Character.isDigit(str.charAt(i)))
					return true;
		}

		return false;
	}

}
