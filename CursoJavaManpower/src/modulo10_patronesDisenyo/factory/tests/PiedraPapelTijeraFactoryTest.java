package modulo10_patronesDisenyo.factory.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modulo10_patronesDisenyo.factory.Papel;
import modulo10_patronesDisenyo.factory.Piedra;
import modulo10_patronesDisenyo.factory.PiedraPapelTijeraFactory;
import modulo10_patronesDisenyo.factory.Tijera;

class PiedraPapelTijeraFactoryTest {

	PiedraPapelTijeraFactory piedra;
	PiedraPapelTijeraFactory papel;
	PiedraPapelTijeraFactory tijera;
	@BeforeEach
	void setUp() throws Exception {
		piedra=new Piedra();
		tijera=new Tijera();
		papel=new Papel();
	}

	@AfterEach
	void tearDown() throws Exception {
		piedra=null;
		tijera=null;
		papel=null;
	}

	@Test
	void testGetInstancePiedra() {
		assertTrue(PiedraPapelTijeraFactory.getInstance(0) instanceof Piedra);
	}
	
	@Test
	void testGetInstanceTijera() {
		assertTrue(PiedraPapelTijeraFactory.getInstance(2) instanceof Tijera);
	}
	
	@Test
	void testGetInstancePapel() {
		assertTrue(PiedraPapelTijeraFactory.getInstance(1) instanceof Papel);
	}
	
	@Test
	void testComparaPiedraPierdePapel() {
		assertEquals(-1, piedra.comparar(papel));
	}
	
	@Test
	void testComparaPapelPierdeTijera() {
		assertEquals(-1, papel.comparar(tijera));
	}

	@Test
	void testComparaTijeraPierdePiedra() {
		assertEquals(-1, tijera.comparar(piedra));
	}
	
	@Test
	void testComparaPiedraGanaTijera() {
		assertEquals(1, piedra.comparar(tijera));
	}
	
	@Test
	void testComparaTijeraGanaPapel() {
		assertEquals(1, tijera.comparar(papel));
	}
	
	@Test
	void testComparaPapelGanaPiedra() {
		assertEquals(1, papel.comparar(piedra));
	}
}
