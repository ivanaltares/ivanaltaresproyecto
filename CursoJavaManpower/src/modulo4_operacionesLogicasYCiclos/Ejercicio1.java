package modulo4_operacionesLogicasYCiclos;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);

		System.out.println("Introducir nota 1: ");
		float nota1=sc.nextFloat();
		System.out.println("Introducir nota 2: ");
		float nota2=sc.nextFloat();
		System.out.println("Introducir nota 3: ");
		float nota3=sc.nextFloat();

		float media=(nota1+nota2+nota3)/3;
		if(media>=7)
			System.out.println("Alumno aprobado. Nota final: "+media);
		else
			System.out.println("Alumno suspenso. Nota final: "+media);
	}

}
