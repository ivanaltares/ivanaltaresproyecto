package modulo2_variables;

public class Mod2_Ejercicio5 {
	public static void main(String[] args) {

		// ejercicios para practicar el autocasteo y la operacion entre distintos tipos de datos numericos

		byte b = 10;
		short s = 20;
		int i = 30;
		long l = 40;

		//siempre se puede convertir de un tipo con menos rango a un tipo con mas rango, pero no al reves, ya que podria dar un numero erroneo al pasarse de rango
		l=s;
		System.out.println("l=s \t"+l);
		b=(byte)s;
		System.out.println("b=s \t"+b);
		l=i;
		System.out.println("l=i \t"+l);
		b=(byte)i;
		System.out.println("b=i \t"+b);
		s=(short)i;
		System.out.println("s=i \t"+s);

	}
}
