package modulo10_patronesDisenyo.Selectstrategy.tests;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modulo10_patronesDisenyo.Selectstrategy.Alumno;
import modulo10_patronesDisenyo.Selectstrategy.SelectStrategy;


public class SelectStrategyTest {
	Alumno aluVacio					;
	Alumno aluConCodigo				;
	Alumno aluConNombre				;	
	Alumno aluConNombreYapellido	;
	Alumno aluConEstudio			;
	Alumno aluConNombreEstudiosLink ;
	@Before
	public void setUp() throws Exception {
		aluVacio				= new Alumno()									;
		aluConCodigo 			= new Alumno(10)								;
		aluConNombre 			= new Alumno(0, "Gabriel",null, null, null)		;
		aluConNombreYapellido 	= new Alumno(0, "Gabriel", "Casas", null, null)	;	
		aluConEstudio 			= new Alumno(0, null, null, "DAM", null)		;
		aluConNombreEstudiosLink =new Alumno(0, "Gabriel", null, "DAM", "http://gitlab/gcasas1972")		;
	}

	@After
	public void tearDown() throws Exception {
		aluVacio	 			= null;
		aluConCodigo 			= null;
		aluConNombre 			= null;
		aluConNombreYapellido 	= null;	
		aluConEstudio 			= null;
		aluConNombreEstudiosLink =null;

		
	}
	@Test
	public void testGetSqlVacio(){
		 assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos",
				 SelectStrategy.getSql(aluVacio));
	}
	@Test
	public void testGetSqlNull(){
		 assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos",
				 SelectStrategy.getSql(null));
	}

	@Test
	public void testGetSqlConCodigo() {
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos where alu_id=10",
					SelectStrategy.getSql(aluConCodigo));
	}
	
	@Test
	public void testGetSqlConNombre() {
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos where alu_nombre=Gabriel",
					SelectStrategy.getSql(aluConNombre));
	}
	
	@Test
	public void testGetSqlConNombreApellido() {
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos where alu_nombre=Gabriel and alu_apellido=Casas",
					SelectStrategy.getSql(aluConNombreYapellido));
	}

	@Test
	public void testGetSqlConEstudio() {
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos where alu_estudios='DAM'",
					SelectStrategy.getSql(aluConEstudio));
	}
	
	@Test
	public void testGetSqlConNombreEstudiosLink() {
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos where alu_nombre=Gabriel and alu_estudios='DAM' and alu_linkgit='http://gitlab/gcasas1972'",
					SelectStrategy.getSql(aluConNombreEstudiosLink));
	}
	
}
